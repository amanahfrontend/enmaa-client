import {SharedModuleModule} from './shared-module/shared-module.module';
// import {SignalRService} from './shared-module/signal-r.service';
import {RoutingModule, RoutingComponents} from './router.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions} from '@angular/http';
// import {SpinnerModule} from 'angular2-spinner/src/';


import {MessageService} from 'primeng/components/common/messageservice';
import {UtilitiesService} from "./api-module/services/utilities/utilities.service";

let token = '';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationServicesService} from "./api-module/services/authentication/authentication-services.service";

import {LocationStrategy, HashLocationStrategy} from '@angular/common'

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    // SpinnerModule,
    SharedModuleModule.forRoot()
    // SignalRModule.forRoot(createConfig)
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    BaseRequestOptions, UtilitiesService, MessageService, AuthenticationServicesService],
  bootstrap: [AppComponent],

})
export class AppModule {
}
