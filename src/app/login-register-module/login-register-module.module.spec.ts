import { LoginRegisterModuleModule } from './login-register-module.module';

describe('LoginRegisterModuleModule', () => {
  let loginRegisterModuleModule: LoginRegisterModuleModule;

  beforeEach(() => {
    loginRegisterModuleModule = new LoginRegisterModuleModule();
  });

  it('should create an instance', () => {
    expect(loginRegisterModuleModule).toBeTruthy();
  });
});
