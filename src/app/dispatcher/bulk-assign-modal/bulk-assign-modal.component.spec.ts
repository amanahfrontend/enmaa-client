import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkAssignModalComponent } from './bulk-assign-modal.component';

describe('BulkAssignModalComponent', () => {
  let component: BulkAssignModalComponent;
  let fixture: ComponentFixture<BulkAssignModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkAssignModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkAssignModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
