import { LookupService } from './services/lookup-services/lookup.service';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { AuthenticationServicesService } from './services/authentication/authentication-services.service';
// import { fakeBackendProvider } from './services/fake-backend';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule,BaseRequestOptions } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  providers: [BaseRequestOptions,AuthGuardGuard,LookupService],

  declarations: []
})
export class ApiModuleModule { }
