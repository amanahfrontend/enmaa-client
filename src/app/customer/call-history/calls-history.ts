import {Component, OnInit, OnDestroy, ViewChild} from "@angular/core";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router, NavigationEnd} from "@angular/router";
import {Message} from "primeng/components/common/message";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {CustomerCrudService} from "../../api-module/services/customer-crud/customer-crud.service";
import {Subscription} from "rxjs";
import {SearchComponent} from "../../shared-module/search/search.component";

@Component({
  selector: 'calls-history',
  templateUrl: './calls-history.html',
  styleUrls: ['./calls-history.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class CallsHistory implements OnInit, OnDestroy {
  calls: any[];
  callsSubscrption: Subscription;
  searchSubscription: Subscription;
  content: any;
  roles: string[];
  // statusSubscription: Subscription;
  warningMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  notificationSubscription: Subscription;

  constructor(private lookUp: LookupService, private modalService: NgbModal, private router: Router, private auth: AuthenticationServicesService, private utilities: UtilitiesService, private customerService: CustomerCrudService) {
  }

  ngOnInit() {
    /*init new action object to be filled and send later*/
    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
        if (value) {
          this.searchComponent.searchText = value;
          this.utilities.routingFromAndHaveSearch = false;
          this.searchByValue(value);
        }
        else if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch && this.utilities.currentSearch.searchType == 'call') {
          this.searchComponent.searchText = this.utilities.currentSearch.searchText;
          this.utilities.routingFromAndHaveSearch = false;
          this.searchByValue(this.utilities.currentSearch.searchText);
        }
        else {
          this.getAllCalls();
        }
      },
      err => {
        console.log(err);
      });
  }

  ngOnDestroy() {
    this.callsSubscrption && this.callsSubscrption.unsubscribe();
    this.notificationSubscription && this.notificationSubscription.unsubscribe();
    this.utilities.routingFromAndHaveSearch = false;
    this.utilities.setSavedNotificationText('');
  }

  getAllCalls() {
    this.toggleLoading = true;
    this.callsSubscrption = this.lookUp.getCallsHistory().subscribe((callsHistory) => {
        this.toggleLoading = false;
        this.calls = callsHistory;
        //console.log(this.calls);
        this.warningMessage = [];
        this.warningMessage.push({
          severity: "info",
          summary: "DropDown Arrow",
          detail: "You Can Add new action by click on the dropdown arrow and choose Add Action"
        })
      },
      err => {
        // //console.log(err);
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get Calls history due to server error"
        })
      });
  }

  searchByValue(value) {
    this.toggleLoading = true;
    //console.log(value);
    this.utilities.currentSearch.searchType = 'call';
    this.searchSubscription = this.customerService.GoSearchCustomer(value).subscribe((searchResult) => {
        this.calls = searchResult;
        this.toggleLoading = false;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to find item du to server error!'
        });
        this.toggleLoading = false;
      })
  }


}
